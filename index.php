<?php
// Datenbankverbindung herstellen
include("config.inc.php");

function jsonResponse($status, $message, $data=array()) {
    $response = array(
        "status" => $status,
        "message" => $message
    );
    if(!empty($data)){
        $response=array_merge($response, $data);
    }
    header('Content-Type: application/json'); // JSON-Header setzen
    echo json_encode($response);
}

$conn = new mysqli($servername, $username, $password, $dbname);

// Überprüfen, ob die Verbindung erfolgreich war
if ($conn->connect_error) {
    jsonResponse("error", "Connection failed". $conn->connect_error);
    die();
}

function node_exists($node, $conn){
  $sql = "SELECT * FROM `nodes` WHERE `node` LIKE '".mysqli_real_escape_string($conn, $node)."'";
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    // Ausgabe der Daten jeder Zeile
    while($row = $result->fetch_assoc()) {
      return $row;
    }
  } else {
    return FALSE;
  }
}

// Daten aus dem Formular erfassen und in der Datenbank speichern
if (!empty($_REQUEST["check_node"])) {
    $node = $_REQUEST["check_node"];
    $r=node_exists($node, $conn);
    if(!empty($r)) {
      jsonResponse("success", "Node exists", array("nick"=>$r["nick"], "since"=>$r["datetime"]));
    }else{
      jsonResponse("error", "Node does not exists");
    }
}else if (!empty($_REQUEST["add_node"])) {
    $node = $_REQUEST["add_node"];
    $nick = $_REQUEST["nick"]??"";
    $r=node_exists($node, $conn);
    if(!empty($r)) {
      jsonResponse("success", "Node already added", array("node"=>$node, "nick"=>$r["nick"], "since"=>$r["datetime"]));
    }else{
      // add node
      $sql = "INSERT INTO `nodes` (`id`, `node`, `datetime`, `IP`, `nick`) VALUES (NULL, '".mysqli_real_escape_string($conn, $node)."', current_timestamp(), NULL, '"
        .mysqli_real_escape_string($conn, $nick)."')";
      if ($conn->query($sql) === TRUE) {
        jsonResponse("success", "Node successfully added", array("node"=>$node, "nick"=>$nick));
      } else {
        jsonResponse("error", "Error: " . $sql . "<br>" . $conn->error);
      }
    }
}else if (isset($_REQUEST["list"])) {
    $sql = "SELECT * FROM `nodes` ORDER BY `datetime`";
    $result = $conn->query($sql);
    $out=array();
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $out[]=$row;
      }
      jsonResponse("success", "This is the list of nodes sorted by date", $out);
      } else {
        jsonResponse("error", "No nodes found");
    }
}else{
    ?><!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Goldkarpfen Node list</title>
        <style>
          /* Reset some default styles for better consistency */
            body, h1, h2, p, ul, li {
                margin: 0;
                padding: 0;
            }

            /* Set a background color and text color */
            body {
                background-color: #f7f7f7;
                font-family: Arial, sans-serif;
                color: #333;
            }

            /* Style the header */
            header {
                background-color: #292b2c;
                color: #ffffff;
                padding: 20px 0;
                text-align: center;
            }

            header h1 {
                font-size: 36px;
                margin-bottom: 10px;
            }

            nav ul {
                list-style-type: none;
            }

            nav ul li {
                display: inline;
                margin-right: 20px;
            }

            nav ul li a {
                text-decoration: none;
                color: #ffffff;
                font-weight: bold;
            }

            /* Style the main content */
            main {
                padding: 20px;
            }

            section {
                background-color: #ffffff;
                border: 1px solid #ddd;
                padding: 20px;
                margin-bottom: 20px;
            }

            /* Style the footer */
            footer {
                background-color: #292b2c;
                color: #ffffff;
                text-align: center;
                padding: 10px 0;
            }

            /* Style links */
            a {
                color: #0073e6;
                text-decoration: none;
            }

            a:hover {
                text-decoration: underline;
            }

        </style>
    </head>
    <body>
        <header>
        <img class="logo" src="Goldkarpfen_logo.jpg" style="height: 59px;
            float: left;
            position: absolute;
            top: 0px;
            left: 0px;">
            <nav>
                <ul>
                    <li><h3 style="padding: 0;
                        margin: 0;
                        display: inline;">Goldkarpfen Tracker</h3>
                     | <a href="https://gitlab.com/rubo77/Goldkarpfen">Source on GitLab</a>
                     | <a href="datenschutz.html">DSGVO</a>
                     | <a href="datenschutz.html#impressum">Impressum</a></li>
                </ul>
            </nav>
        </header>
    
        <main>
            <section id="intro">
                <h2>List</h2>
                <p>    
                call the site with your parameters<br>
                - add node: https://goldkarpfen.eclabs.de/?add_node=1234567890123456789012345678901234567890&nick=xyz<br>
                - check node: https://goldkarpfen.eclabs.de/?check_node=1234567890123456789012345678901234567890"<br>
                - list nodes: https://goldkarpfen.eclabs.de/?list</p>
            </section>
        </main>
    
        <footer>
            <p>&copy; 2023 Goldkarpfen</p>
        </footer>
    </body>
    </html>
    <?
}

$conn->close();
