
--
-- Tabellenstruktur für Tabelle `nodes`
--

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL,
  `node` varchar(255) DEFAULT NULL,
  `nick` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT current_timestamp(),
  `IP` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
